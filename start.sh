#!/bin/bash
CONTAINER_IP=$(hostname -i)
MARIADB_HOST_ADDRESS=${MARIADB_HOST_ADDRESS:=}
ZOOKEEPER_HOST_ADDRESS=${ZOOKEEPER_HOST_ADDRESS:=}
TRANSPORT_PUBLIC_INTERFACE=${TRANSPORT_PUBLIC_INTERFACE:=$CONTAINER_IP}
CASSANDRA_ADDRESSES=${CASSANDRA_ADDRESSES:=}
# Debugging
echo "mariadb: $MARIADB_HOST_ADDRESS" >> /var/testi.txt
echo "zoo: $ZOOKEEPER_HOST_ADDRESS" >> /var/testi.txt
echo "public: $TRANSPORT_PUBLIC_INTERFACE" >> /var/testi.txt
# Set nosql db to cassandra
sed -i '3s/.*/nosql_db_provider_name=cassandra/' /etc/kaa-node/conf/nosql-dao.properties
# Set cassandra host
sed -i "s/\(node_list *= *\).*/\1$CASSANDRA_ADDRESSES/" /usr/lib/kaa-node/conf/common-dao-cassandra.properties
# Set mariadb host
sed -i "s/\(jdbc_url *= *\).*/\1jdbc\:mysql\:failover\:\/\/$MARIADB_HOST_ADDRESS\:3306\/kaa/" /usr/lib/kaa-node/conf/admin-dao.properties
sed -i "s/\(jdbc_host_port *= *\).*/\1$MARIADB_HOST_ADDRESS\:3306/" /usr/lib/kaa-node/conf/sql-dao.properties
# Set zookeeper host
sed -i "s/\(zk_host_port_list *= *\).*/\1$ZOOKEEPER_HOST_ADDRESS\:2181/" /usr/lib/kaa-node/conf/kaa-node.properties
# Set kaa-node
sed -i "s/\(transport_public_interface *= *\).*/\1$TRANSPORT_PUBLIC_INTERFACE/" /usr/lib/kaa-node/conf/kaa-node.properties